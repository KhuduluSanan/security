package security.example.security.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import security.example.security.dto.JwtDto;
import security.example.security.dto.LoginDto;
import security.example.security.dto.RegisterDto;
import security.example.security.service.UserService;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@RequestMapping("/logins")
public class UserController {

    private final UserService userService;

//    @PostMapping("/save") //hamiya aciq
//    public String userSave(@RequestBody UserDto userDto) {
//        return userService.saved(userDto);
//    }

    @PostMapping("/login") //hamiya aciq
    public String login(@RequestBody LoginDto loginDto, @RequestHeader("Authorization") String token) {
        return userService.login(loginDto,token);
    }

    @PostMapping("/user") //USER e aciq
    public String user() {
        return "user ile giris etdiniz";
    }

    @PostMapping("/admin") //ADMIN e aciq
    public String admin() {
        return "admin ile giris etdiniz";
    }

    @PostMapping("/register") //hamiya aciq
    public ResponseEntity<JwtDto> token(@RequestBody RegisterDto registerDto) {
        return new ResponseEntity<JwtDto>(userService.token(registerDto), OK);
    }


}
