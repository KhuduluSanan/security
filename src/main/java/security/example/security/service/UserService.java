package security.example.security.service;

import security.example.security.dto.JwtDto;
import security.example.security.dto.LoginDto;
import security.example.security.dto.RegisterDto;
import security.example.security.dto.UserDto;
import security.example.security.entity.User;

public interface UserService {

//    String saved(UserDto userDto);

    String login(LoginDto loginDto,String token);

    JwtDto token(RegisterDto registerDto);
}
