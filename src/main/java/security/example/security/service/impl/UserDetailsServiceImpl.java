package security.example.security.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import security.example.security.entity.User;
import security.example.security.repo.UserRepo;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userdb = userRepo.findByName(username).orElseThrow(() -> new RuntimeException("Not found data"));
        userRepo.save(userdb);
        return org.springframework.security.core.userdetails.User.builder()
                .username(userdb.getName())
                .password(userdb.getPassword())
                .authorities(userdb.getAuthority().name())
                .build();
    }

}
