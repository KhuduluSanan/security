package security.example.security.service.impl;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import security.example.security.config.AppConfiguration;
import security.example.security.config.CustomJwtParser;
import security.example.security.dto.JwtDto;
import security.example.security.dto.LoginDto;
import security.example.security.dto.RegisterDto;
import security.example.security.entity.User;
import security.example.security.enums.Type;
import security.example.security.repo.UserRepo;
import security.example.security.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final AppConfiguration appConfiguration;
    private final CustomJwtParser customJwtParser;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

//    @Override
//    public String saved(UserDto userDto) {
//        User map = User.builder()
//                .password(bCryptPasswordEncoder.encode(userDto.getPassword()))
//                .name(userDto.getName())
//                .authority(userDto.getAuthority())
//                .build();
//        userRepo.save(map);
//        return "Saved";
//    }

    @Override
    public String login(LoginDto loginDto, String token) {
        User map = appConfiguration.getMapper().map(loginDto, User.class);
        if (userRepo.findByName(loginDto.getName()).isPresent()) {
            if (bCryptPasswordEncoder.encode(loginDto.getPassword())
                    .equals(bCryptPasswordEncoder.encode(map.getPassword()))) {
                return "Login you :)";
            }
        }
        Claims claims = customJwtParser.parseToken(token);
        String authority = claims.get("authority", String.class);
        return "You are not Register Mr/Miss";

    }

    @Override
    public JwtDto token(RegisterDto registerDto) {
        userRepo.findByName(registerDto.getUserName()).ifPresent(user -> {
            throw new RuntimeException("User already exsist");
        });
        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            throw new RuntimeException("Password are different !");
        }
        User user = User.builder()
                .name(registerDto.getUserName())
                .password(bCryptPasswordEncoder.encode(registerDto.getPassword()))
                .authority(Type.valueOf(registerDto.getAuthority()))
                .build();
        User saveUser = userRepo.save(user);
        String token = customJwtParser.generateToken(saveUser);
        return JwtDto.builder()
                .jwt(token)
                .build();
    }
}
