package security.example.security.dto;

import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;
import security.example.security.enums.Type;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    String name;
    String password;
    Type authority;
}
