package security.example.security.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import security.example.security.entity.User;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findByName(String name);
}
