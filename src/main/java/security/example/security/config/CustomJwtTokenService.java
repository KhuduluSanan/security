package security.example.security.config;

import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CustomJwtTokenService {
    private final CustomJwtParser customJwtParser;
    private final UserDetailsService userDetailsService;

    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader("Authorization"))
                .filter(header -> isBearerToken(header))
                .map(header -> getAuthentication(header));
    }

    private boolean isBearerToken(String header) {
        return header.toLowerCase().startsWith("bearer ");
    }

    private Authentication getAuthentication(String header) {
        String token = header.substring("bearer ".length()).trim();
        Claims claims = customJwtParser.parseToken(token);
        if (claims.getExpiration().before(new Date())) {
            return null;
        }
        return getAuthentication(claims);
    }

    private Authentication getAuthentication(Claims claims) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(claims.getSubject());
        String authority = claims.get("authority", String.class);
        List<GrantedAuthority> authorities = List.of
                (new SimpleGrantedAuthority(authority));

        return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
    }
}
