package security.example.security.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Slf4j
@RequiredArgsConstructor
public class CustomAuthSecurityAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain,
        HttpSecurity> {
    private final CustomJwtTokenService customJwtTokenService;

    @Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        log.info("start configre filter");
        httpSecurity.addFilterBefore(new CustomSecurityFilter(customJwtTokenService)
                , UsernamePasswordAuthenticationFilter.class);
    }
}
