package security.example.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private final CustomJwtTokenService customJwtTokenService;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable);
        http.cors(AbstractHttpConfigurer::disable);
        http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/logins/login").permitAll());
//        http.authorizeHttpRequests(auth -> auth.requestMatchers("/login/save").permitAll());

        http.authorizeHttpRequests(auth -> auth.requestMatchers("/logins/register").permitAll());
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/logins/user").hasAuthority("USER"));
        http.authorizeHttpRequests(auth -> auth.requestMatchers("/logins/admin").hasAuthority("ADMIN"));
        http.authorizeHttpRequests(auth -> auth.anyRequest().authenticated());

        http.httpBasic(withDefaults());
        http.apply(new CustomAuthSecurityAdapter(customJwtTokenService));
        return http.build();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
