package security.example.security.exception;

public class DataIntegrityViolationException extends Throwable {

    public DataIntegrityViolationException(String message) {
        super(message);
    }
}
